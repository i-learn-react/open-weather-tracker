import calculateMode from '../Utils/calculateMode';
import calculateMean from '../Utils/calculateMean';


class Tracker {
    constructor() {
        this.tracker = [];
    }

    insert(value) {
        this.tracker.push(value);
        return this;
    } 
    
    showMin = () => Math.min(...this.tracker);
    
    showMax = () => Math.max(...this.tracker);
    
    showMean = () => calculateMean(this.tracker);

    showMode = () => calculateMode(this.tracker);
}

export default Tracker;