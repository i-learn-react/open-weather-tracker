class DailyForecast {
    constructor({
        dt,
        humidity,
        temp: {
            day,
            night,
            morn
        }
    }){
        this.timestamp = dt;
        this.humidity = humidity;
        this.morningTemp = morn;
        this.nightTemp = night;
        this.dayTemp = day;
    }
}

export default DailyForecast;