import calculateMode, { NO_MODE } from './calculateMode';

describe('calculateMode test suite', () => {
    test('for empty list should return "no mode" ', () => {
        expect(calculateMode([])).toBe(NO_MODE);
    });

    test('for one item list should return item value', () => {
        expect(calculateMode([2])).toBe(2);
    });

    test('for [1,2] list should return "no mode"', () => {
        expect(calculateMode([1,2])).toBe(NO_MODE);
    });

    test('for [2,2,3] list should return 2 ', () => {
        expect(calculateMode([2,2,3])).toBe(2);
    });
    
    test('for [2.2, 2.2, 3] list should return 2.2 ', () => {
        expect(calculateMode([2.2, 2.2, 3])).toBe(2.2);
    });
});