export const makeApiRequest = async (url) => {
    try {
        const result = await fetch(url);
        if (!result.ok) {
            const parsedResponse = await result.json();
            throw (parsedResponse)
                ? parsedResponse
                : { cod: result.status, message: result.statusText };
        } 
        return await result.json();
    } catch ({cod, message}) {
        throw new Error(`Error ${cod}: ${message}`);        
    }
}
