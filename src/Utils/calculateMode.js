const countValues = (all, current) => {
    return all[current] ? { ...all, [current]: all[current] + 1 } : { ...all, [current]: 1 };
}

export const NO_MODE = 'no mode';

function calculateMode(list) {
    if (list.length === 0) {
        return NO_MODE;
    }
    if (list.length === 1) {
        return list[0];
    }
    const countedValues = list.reduce(countValues, {});
    let sortable = Object.entries(countedValues);

    sortable.sort(function(a, b) {
        return b[1] - a[1];
    });
    return sortable[0][1] === sortable[1][1] ? NO_MODE : Number(sortable[0][0]);
}

export default calculateMode;