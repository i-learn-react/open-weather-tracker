const { default: calculateMean } = require("./calculateMean");

describe('calculateMean test suite', () => {
    test('for empty list should return 0', () => {
        expect(calculateMean([])).toBe(0);
    });

    test('for [2] should return 2', () => {
        expect(calculateMean([2])).toBe(2);
    });
    
    test('for [2, 2] should return 2', () => {
        expect(calculateMean([2, 2])).toBe(2);
    });
    
    test('for [2, 4] should return 2', () => {
        expect(calculateMean([2, 4])).toBe(3);
    });
});