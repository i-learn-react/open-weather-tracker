import Tracker from "../Models/Tracker";

function prepareTrackers(list) {
    const trackers = {
        humidity: new Tracker(),
        morningTemp: new Tracker(),
        nightTemp: new Tracker(),
        dayTemp: new Tracker(),
    };
    return list.reduce((all, day) => (
        {
            humidity: all.humidity.insert(day.humidity),
            morningTemp: all.morningTemp.insert(day.morningTemp),
            nightTemp: all.nightTemp.insert(day.nightTemp),
            dayTemp: all.dayTemp.insert(day.dayTemp),
        }
    ), trackers)
}

export default prepareTrackers;