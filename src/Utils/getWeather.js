import { makeApiRequest } from './makeRequest';

async function getWeather(city) {
  const API_KEY = process.env.REACT_APP_API_KEY;
  
  const getCityData = apiKey =>
    city =>
      `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apiKey}`;
  
  const getForecast = apiKey =>
    (lat, lon) =>
      `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&exclude={hourly}&appid=${apiKey}`;
  
  const cityUrl = getCityData(API_KEY)(city);
  const forecastUrl = getForecast(API_KEY);

  let coord;

  try {
    const cityResponse = await makeApiRequest(cityUrl);
    coord = cityResponse.coord;
  } catch (error) {
    throw error;    
  }

  try {
    const {daily} = await makeApiRequest(forecastUrl(coord.lat, coord.lon));
    return daily;
  } catch (error) {
        throw error;    
  }
}

export default getWeather;