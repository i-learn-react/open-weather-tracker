const sumAll = (all, current) => all + current;

function calculateMean(list) {
    return list.length > 0 ? (list.reduce(sumAll, 0) / list.length).toFixed(2) : 0;
}

export default calculateMean;