export const TABLE_HEADERS = ['Humidity', 'Morning Temp', 'Night Temp', 'Day Temp'];
export const STATS = [
    {
        method: 'showMin',
        label: 'Min'
    },
    {
        method: 'showMax',
        label: 'Max'
    },
    {
        method: 'showMean',
        label: 'Mean'
    },
    {
        method: 'showMode',
        label: 'Mode'
    },
];