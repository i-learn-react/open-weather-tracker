import React, { Component, Fragment } from 'react';

import getWeather from '../Utils/getWeather';
import prepareTrackers from '../Utils/prepareTrackers';
import DailyForecast from '../Models/DailyForecast';
import RenderDay from './RenderDay';
import RenderStat from './RenderStat';
import Form from './Form';
import Table from './Table';
import { TABLE_HEADERS, STATS } from './constants';
import './weather.css';

class Main extends Component {
    state = {
        days: [],
        hasError: false,
        errorMessage: '',
        trackers: null,
    }

    handleRequest = (city) => {
        getWeather(city)
            .then(days => days.map(day => new DailyForecast(day)))
            .then(days => [...days].slice(0,5))
            .then(days => this.setState({days, hasError: false, trackers: prepareTrackers(days)}))
            .catch(({message}) => this.setState(
                { hasError: true, errorMessage: message, days: [] }
            ));
    }
    
    render() {
        const { days, hasError, errorMessage, trackers } = this.state;
        return (
            <div className='weather'>
                <Form onSend={this.handleRequest} />
                {hasError && <p>{errorMessage}</p>}
                {!hasError && days.length === 0 && <p> No data!</p>}
                {days.length !== 0 && (
                    <Fragment>
                        <Table
                            header={
                                <Fragment>
                                    {['Day', ...TABLE_HEADERS].map(header => <th key={header}>{header}</th>)}
                                </Fragment>
                            }
                        >
                            {days.map(day => <RenderDay key={day.timestamp} day={day} />)}
                        </Table>
                        <Table
                            header={
                                <Fragment>
                                    {['Stat', ...TABLE_HEADERS].map(header => <th key={header}>{header}</th>)}
                                </Fragment>
                            }
                        >
                            {
                                STATS.map((stat, i) =>
                                    <RenderStat key={i} stat={stat} trackers={trackers} />
                                )
                            }
                        </Table>
                    </Fragment>
                )}
            </div>
        )
    }
}

export default Main;
