import React from 'react';
import PropTypes from 'prop-types';

const RenderStat = ({ stat, trackers }) => {
    const { label, method} = stat;
    return (
        <tr>
            <td>{label}</td>
            <td>{trackers.humidity[method]()}</td>
            <td>{trackers.morningTemp[method]()}</td>
            <td>{trackers.nightTemp[method]()}</td>
            <td>{trackers.dayTemp[method]()}</td>
        </tr>
    );
};

RenderStat.propTypes = {
    stat: PropTypes.shape({
        label: PropTypes.string,
        method: PropTypes.string,
    }),
    trackers: PropTypes.shape({}),
};

export default RenderStat;