import React from 'react';

const Form = ({onSend}) => {
    const [city, setCity] = React.useState('');
    return (
        <form onSubmit={e => {
            e.preventDefault();
            onSend(city);
            setCity('');
        }}>
            <label>
                City
					<input value={city} onChange={e => setCity(e.target.value)} />
            </label>
            <button type="submit">Get Weather!</button>
        </form>
    );
};

export default Form;