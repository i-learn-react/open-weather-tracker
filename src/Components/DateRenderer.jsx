import React from 'react';
import moment from 'moment';

const DateRenderer = ({date, format}) => {
    return (
        <span>
            {moment.unix(date).format(format)}
        </span>
    );
};

export default DateRenderer;