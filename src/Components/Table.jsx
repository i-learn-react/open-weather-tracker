import React from 'react';
import './table.css';

const Table = ({children, header}) => {
    return (
        <table className='table'>
            <thead>
                <tr>
                    {header}
                </tr>
            </thead>
            <tbody>
                {children}
            </tbody>
        </table>
    );
};

export default Table;