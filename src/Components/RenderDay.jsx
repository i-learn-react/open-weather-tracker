import React from 'react';
import PropTypes from 'prop-types';
import DailyForecast from '../Models/DailyForecast';
import DateRenderer from './DateRenderer';

const RenderDay = ({ day }) => {
    const { timestamp, humidity, morningTemp, nightTemp, dayTemp } = day;
    return (
        <tr>
            <td><DateRenderer date={timestamp} format='DD.MM.YYYY' /></td>
            <td>{humidity}</td>
            <td>{morningTemp}</td>
            <td>{nightTemp}</td>
            <td>{dayTemp}</td>
        </tr>
    );
};

RenderDay.propTypes = {
    day: PropTypes.instanceOf(DailyForecast).isRequired,
};

export default RenderDay;